import React from 'react'
import { useParams } from 'react-router';
import {Link} from 'react-router-dom'

export default function Account() {
    let { id } = useParams();

  return (
    <div>
      <h2>Accounts</h2>
      <ul>
          <li><Link to="/account/netflix">Netflix</Link></li>
          <li> <Link to="/account/zillow-group">Zillow Group</Link></li>
          <li> <Link to="/account/yahoo">Yahoo</Link></li>
          <li><Link to="/account/modus-create">Modus Create</Link></li>
      </ul>
      <h3>
        ID: <span className="id">{id}</span>
      </h3>
    </div>
  );
}
