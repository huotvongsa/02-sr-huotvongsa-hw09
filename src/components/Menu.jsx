import React,{useState} from "react";
import { Navbar, Form, Container, Nav,FormControl,Button } from "react-bootstrap";
import {Link} from 'react-router-dom'

export default function Menu() {
    const [id,setId] = useState(10)
    
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Container>
        <Navbar.Brand href="#home">React-Router</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/video">Video</Nav.Link>
            <Nav.Link as={Link} to={`/account/${id}`}>Account</Nav.Link>
            <Nav.Link as={Link} to="/welcome">Welcome</Nav.Link>
            <Nav.Link as={Link} to="/auth">Auth</Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
        </Container>
        
      </Navbar>
    </div>
  );
}
