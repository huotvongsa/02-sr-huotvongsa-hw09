import { Container } from 'react-bootstrap';
import Menu from './components/Menu'
import Home from './pages/Home'
import Video from './pages/Video'
import Account from './pages/Account'
import Login from './pages/Login'
import Detail from './pages/Detail'
import ProtectedRoute from './pages/ProtectedRoute';
import React,{useState} from 'react';
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom'

function App() {

  const [isSignin, setSignin] = useState(false);
    function onSignin(){
        setSignin(!isSignin);
    }
  return (

      <Router>
        <Route onSignin={onSignin} isSignin={isSignin} component={Menu}/>
        <Switch>
          <Container>
            <Route exact path="/" component={Home} />
            <Route path="/video" component={Video} />
            <Route path="/account" component={Account} />
            <Route path="/welcome"  component={Login} />
            <Route path ="/detail" component={Detail}/>
            <ProtectedRoute isSignin = {isSignin} component ={Login} path="/welcome" exact/>
          </Container>
        </Switch>
      </Router>
    
  );
}

export default App;
